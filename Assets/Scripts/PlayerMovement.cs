﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerBoundary
{
	public float xMin, xMax, yMin, yMax;
}

public class PlayerMovement : MonoBehaviour
{
	public float speed;
	public PlayerBoundary playerBoundary;
	public float TurnMultiplier;
	public float RollMultiplier;
	public float StartLocation;

	void FixedUpdate ()
	{
		float moveLeftRight = Input.GetAxis ("Horizontal");
		float moveUpDown = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveLeftRight, moveUpDown, StartLocation);
		GetComponent<Rigidbody>().velocity = movement * speed;

		GetComponent<Rigidbody> ().position = new Vector3
			(
				Mathf.Clamp (GetComponent<Rigidbody> ().position.x, playerBoundary.xMin, playerBoundary.xMax),
				//Mathf.Clamp (GetComponent<Rigidbody> ().position.y, playerBoundary.yMin, playerBoundary.yMax),
				StartLocation
			 );

		GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f, GetComponent<Rigidbody>().velocity.x * -RollMultiplier, GetComponent<Rigidbody>().velocity.x * -TurnMultiplier);
		//GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f, GetComponent<Rigidbody>().velocity.z * -(tilt * 2), 0.0f);
	}
}
