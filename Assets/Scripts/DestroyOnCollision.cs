﻿using UnityEngine;
using System.Collections;

public class DestroyOnCollision : MonoBehaviour
{
	public int scoreValue;
	private GameController gameController;
	public GameObject explosion;
	
	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent <GameController> ();
		}
	}
		
	void OnTriggerEnter(Collider other) 
	{
		if (other.tag == "DestroyBoundary")
		{
			return;
		}

		if (other.tag == "Player")
		{
			if (gameObject.tag == "Boss")
			{
				Destroy (gameObject);
				gameController.GameOver ();
				return;
			}

			else if (gameObject.tag == "PowerUp")
			{
				Destroy (gameObject);
				gameController.AddLives ();
				return;
			}

			else if (gameController.lives >= 1 )
			{
				gameController.MinusLives();
				Destroy (gameObject);
				return;
			}
			else if(gameController.lives < 1)
			{
				//gameController.AddLives();
				Destroy (gameObject);
				//gameObject.SetActive(false);	
				gameController.GameOver ();
			}
			Instantiate(explosion);
		}

		if (other.tag == "Endzone")
		{
			if (gameObject.tag == "Boss")
			{
				Destroy (gameObject);
				gameController.GameOver ();
				return;
			}

			else if (gameController.lives >= 1 )
			{
				gameController.MinusLives();
				Destroy(gameObject);
				return;
			}
			else if(gameController.lives < 1)
			{
				//gameController.AddLives();
				Destroy(gameObject);
				gameController.GameOver ();
			}
		}

		if (gameObject.tag == "FriendlyBullet")
		{
			if (other.tag == "Enemy")
			{
				Destroy (other.gameObject);
				Destroy (gameObject);
				return;
			}
			else if (other.tag == "PowerUp")
			{
				Destroy (other.gameObject);
				Destroy (gameObject);
				gameController.AddLives ();
				return;
			}
			else if(other.tag == "EnemyBullet")
			{
				Destroy (other.gameObject);
				Destroy (gameObject);
				//Instantiate(explosion, transform.position, transform.rotation);
				return;
			}
			else if(other.tag == "Boss")
			{
				if (gameController.bossLives >= 1 )
				{
					gameController.AddBossLives();
					Destroy(gameObject);
					return;
				}
				else if(gameController.bossLives < 1)
				{
					//gameController.AddLives();
					Destroy (other.gameObject);
					Destroy(gameObject);
					gameController.Victory ();
				}
			}
		}

		if (other.tag == "Wall")
		{
			if (gameObject.tag == "FriendlyBullet")
			{
				return;
			}
			else if (gameObject.tag == "Enemy")
			{
				Destroy (other.gameObject);
				Destroy (gameObject);
				Instantiate (explosion, transform.position, transform.rotation);
				return;
			}
			else if (gameObject.tag == "Boss")
			{
				Destroy (other.gameObject);
				Instantiate (explosion, transform.position, transform.rotation);
				return;
			}
			else if (gameObject.tag == "EnemyBullet")
			{
				Destroy (gameObject);
				Instantiate (explosion, transform.position, transform.rotation);
				return;
			}
			else
			{
				Destroy (gameObject);
				return;
			}
		}

		if (gameObject.tag == "EnemyBullet")
		{
			if (other.tag == "Boss")
			{
				return;
			}
			if (other.tag == "ShieldShip")
			{
				Destroy (other.gameObject);
				Destroy (gameObject);
				Instantiate (explosion, transform.position, transform.rotation);
				return;
			}
		}

		if (other.tag == "ShieldShip")
		{
			if (gameObject.tag == "Enemy")
			{
				Destroy (other.gameObject);
				Destroy (gameObject);
				Instantiate (explosion, transform.position, transform.rotation);
				return;
			}
		}

		if (gameObject.tag == "Boss")
		{
			if (other.tag == "EnemyBullet")
			{
				return;
			}
		}

		Instantiate(explosion, transform.position, transform.rotation);
		gameController.AddScore (scoreValue);
	}
}
