﻿using UnityEngine;
using System.Collections;

public class ShieldHit : MonoBehaviour {
	public GameObject explosion;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame

		void OnTriggerEnter (Collider other)
		{
		if (other.tag == "EnemyBullet" || other.tag == "Enemy") {
			return;
		}
			
		if (explosion != null) {
			Instantiate (explosion, transform.position, transform.rotation);
		}
	}
}
