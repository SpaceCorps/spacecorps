﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float speed;

	void Start () {
		Vector3 velocity = transform.forward;
		velocity.z = 0f;
		velocity = velocity.normalized * speed;
		GetComponent<Rigidbody> ().velocity = velocity;
	}
}
