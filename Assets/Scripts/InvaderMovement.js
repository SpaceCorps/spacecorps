﻿ #pragma strict
 
 public var leftRight = 1.0;
 public var down = 1.0;
 public var speed = 0.5;
 
 function Start() {
     Cycle();
 }
 
 function Cycle() {
     var dest = transform.position;
     while (true) {
         dest.x += leftRight;
         while (transform.position != dest) {
             transform.position = Vector3.MoveTowards(transform.position, dest, speed * Time.deltaTime);
             yield;
         }
 
         dest.y -= down;
 
         while (transform.position != dest) {
             transform.position = Vector3.MoveTowards(transform.position, dest, speed * Time.deltaTime);
             yield;
         }
 
         dest.x -= leftRight;
 
         while (transform.position != dest) {
             transform.position = Vector3.MoveTowards(transform.position, dest, speed * Time.deltaTime);
             yield;
         }
         
         dest.y -= down;
 
         while (transform.position != dest) {
             transform.position = Vector3.MoveTowards(transform.position, dest, speed * Time.deltaTime);
             yield;
         }
     }
 }