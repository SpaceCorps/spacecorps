﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	public GameObject hazard;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GameObject hazard2;
	public Vector3 spawnValues2;
	public int hazardCount2;
	public float spawnWait2;
	public float startWait2;
	public float waveWait2;

	public GameObject powerUp;
	public Vector3 spawnValues3;
	public int powerUpCount;
	public float spawnWait3;
	public float startWait3;
	public float waveWait3;

	private int score;
	public Text scoreText;
	private string scoreOutput = "Score: ";

	public int lives;
	public Text livesText;
	private string livesOutput = "Lives: ";

	public Text GameOverText;
	public Text RestartText;
	private bool gameOver;
	private bool gameOverVictory;
	private bool restart;

	public int finalScore;
	public int bossLives;
	private string bossLivesOutput = "HP: ";
	private bool bossAwake;
	public int spawnWait4;
	public GameObject finalBoss;
	public Vector3 spawnValues4;


	public GameObject player;
	public AudioSource PlayerDeath;
	public GameObject endzone;

	public AudioSource BackgroundMusic;
	public AudioSource EndMusic;


	void Start ()
	{
		gameOver = false;
		gameOverVictory = false;
		bossAwake = false;
		restart = false;
		GameOverText.text = "";
		RestartText.text = "";
		scoreText.text = "Score: ";
		livesText.text = "Lives: " + lives;
		StartCoroutine (SpawnWaves ());
		StartCoroutine (SpawnWaves2 ());
		StartCoroutine (SpawnWaves3 ());
	}

	void Update ()
	{
		GameState ();

		if (restart)
		{
			if (gameOver)
			{
				if (Input.GetKeyDown (KeyCode.R))
				{
					Application.LoadLevel ("Game_Over");
				}
			}
			if (gameOverVictory)
			{
				if (Input.GetKeyDown (KeyCode.R))
				{
					Application.LoadLevel ("Game_Over_Victory");
				}
			}
		}

		if (Input.GetKeyDown (KeyCode.Escape))
		{
			Application.Quit ();
		}

		if (score == finalScore)
		{
			score = score + 1;
			bossAwake = true;
			StartCoroutine (FinalStage ());
		}
	}
	
	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i = i + 1)
			{
				Vector3 spawnPosition = new Vector3 (spawnValues.x, spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver)
			{
				break;
			}
			if (bossAwake)
			{
				break;
			}
		}
	}

	IEnumerator SpawnWaves2 ()
	{
		yield return new WaitForSeconds (startWait2);
		while (true)
		{
			for (int i = 0; i < hazardCount2; i = i + 1)
			{
				Vector3 spawnPosition2 = new Vector3 (Random.Range (-spawnValues2.x, spawnValues2.x), spawnValues2.y, spawnValues2.z);
				Quaternion spawnRotation2 = Quaternion.identity;
				Instantiate (hazard2, spawnPosition2, spawnRotation2);
				yield return new WaitForSeconds (spawnWait2);
			}
			yield return new WaitForSeconds (waveWait2);

			if (gameOver)
			{
				break;
			}
			if (bossAwake)
			{
				break;
			}
		}
	}

	IEnumerator SpawnWaves3 ()
	{
		yield return new WaitForSeconds (startWait3);
		while (true)
		{
			for (int i = 0; i < powerUpCount; i = i + 1)
			{
				Vector3 spawnPosition3 = new Vector3 (Random.Range (-spawnValues3.x, spawnValues3.x), spawnValues3.y, spawnValues3.z);
				Quaternion spawnRotation3 = Quaternion.identity;
				Instantiate (powerUp, spawnPosition3, spawnRotation3);
				yield return new WaitForSeconds (spawnWait3);
			}
			yield return new WaitForSeconds (waveWait3);

			if (gameOver)
			{
				break;
			}
			if (bossAwake)
			{
				break;
			}
		}
	}


	public void AddLives ()
	{
		lives = lives + 1;
		UpdateLives ();
	}

	public void MinusLives ()
	{
		lives = lives - 1;
		UpdateLives ();
	}

	void UpdateLives ()
	{
		livesText.text = livesOutput + lives;
	}

	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}
		
	void UpdateScore ()
	{
		scoreText.text = scoreOutput + score;
	}

	public void AddBossLives ()
	{
		bossLives = bossLives - 1;
		UpdateBossLives ();
	}

	void UpdateBossLives ()
	{
		GameOverText.text = bossLivesOutput + bossLives;
	}

	public void GameOver ()
	{
		GameOverText.text = "Game Over!";
		gameOver = true;
		PlayerDeath.Play ();
		Destroy (player);
		Destroy (endzone);
	}

	public void Victory ()
	{
		GameOverText.text = "Victory!";
		gameOverVictory = true;

		BackgroundMusic.Stop();
		
		EndMusic.Play();
	}

	public void GameState ()
	{
		if (gameOver)
		{
			RestartText.text = "Press 'R' to Continue";
			restart = true;
		}
		if (gameOverVictory)
		{
			RestartText.text = "Press 'R' to Continue";
			restart = true;
		}
	}


	IEnumerator FinalStage ()
	{
		yield return new WaitForSeconds (spawnWait4);
		while (true)
		{
			Vector3 spawnPosition4 = new Vector3 (spawnValues4.x, spawnValues4.y, spawnValues4.z);
			Quaternion spawnRotation4 = Quaternion.identity;
			Instantiate (finalBoss, spawnPosition4, spawnRotation4);
			break;
		}
	}
}