﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			Application.Quit ();
		}

		if (Input.GetKeyDown (KeyCode.R))
		{
			Application.LoadLevel ("Game Redo");
		}
	}
}
