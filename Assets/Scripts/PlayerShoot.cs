﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

	public GameObject bolt;
	public Transform boltSpawn;
	public float fireRate;
	//public GameObject missile1; //Missile
	//public GameObject missile2; //Missile

	//private float missileSpeed;
	private float nextFire;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire1")&& Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			Instantiate (bolt, boltSpawn.position, boltSpawn.rotation);
			GetComponent<AudioSource>().Play ();
		}
		if (Input.GetKey ("space")&& Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			GameObject bul = (GameObject)Instantiate (bolt, boltSpawn.position, Quaternion.LookRotation(transform.up,Vector3.forward));

			GetComponent<AudioSource>().Play ();
		}
		/*if (Input.GetButton ("Fire2")&& Time.time > nextFire)  //Missile
		{
			missile1.GetComponent<Rigidbody>().isKinematic = false;
			missile1.GetComponent<Movement>().enabled = true;
			missile1.transform.parent = null;
			missile2.GetComponent<Rigidbody>().isKinematic = false;
			missile2.GetComponent<Movement>().enabled = true;
			missile2.transform.parent = null;
			nextFire = Time.time + fireRate;
			//Transform (bolt, boltSpawn.position, boltSpawn.rotation); 
		}	
		*/
	}
}
