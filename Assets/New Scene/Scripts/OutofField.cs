﻿using UnityEngine;
using System.Collections;

public class OutofField : MonoBehaviour
{
	void OnTriggerExit (Collider other) 
	{
		Destroy(other.gameObject);
	}
}