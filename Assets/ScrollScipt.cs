﻿using UnityEngine;
using System.Collections;

public class ScrollScipt : MonoBehaviour {
	Renderer rend;
	public float speed = 1f;
	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		rend.material.SetTextureOffset ("_MainTex", new Vector2 (0, Time.time*speed));
	}
}
